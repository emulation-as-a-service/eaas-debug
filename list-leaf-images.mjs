#!/usr/bin/env node

import { argv } from "process";
import { promises as fsPromises } from "fs";
const { opendir } = fsPromises;
import { promisify } from "util";
import { execFile as execFileCallback } from "child_process";
const execFile = promisify(execFileCallback);

const qemuInfo = async (path) => {
    const { stdout } = await execFile("qemu-img", [
        "info",
        "--output=json",
        path,
    ]);
    return JSON.parse(stdout);
};

const fileId = (path) => path?.split(/\//).slice(-1)[0];

const find = async function* (path) {
    for await (const entry of await opendir(path)) {
        const newPath = [path, entry.name].join("/");
        if (entry.isFile()) {
            yield newPath;
        } else if (entry.isDirectory()) {
            try {
                yield* find(newPath);
            } catch (e) {
                console.error(e);
            }
        }
    }
};

const tryMap = async function* (iterable, func) {
    for await (const value of iterable) {
        try {
            yield func(value);
        } catch (e) {
            console.error(e);
        }
    }
};

const collect = async (iterable) => {
    const ret = [];
    for await (const value of iterable) {
        // console.error("...", JSON.stringify(value).slice(0, 70));
        console.error(value);
        ret.push(value);
    }
    return ret;
};

(async () => {
    const [path] = argv.slice(2);

    let res = find(path);
    res = tryMap(res, qemuInfo);
    const images = await collect(res);
    const backings = new Set();
    images.forEach((qemuInfo) => {
        const backing = fileId(qemuInfo["backing-filename"]);
        if (backing) backings.add(backing);
    });
    console.error(backings);
    const notBacked = images.filter((v) => !backings.has(fileId(v.filename)));
    const onlyQcow = notBacked.filter((v) => v.format === "qcow2");
    console.log(onlyQcow.map((v) => v.filename).join("\n"));
})().catch(console.error);
