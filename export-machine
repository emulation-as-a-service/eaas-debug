#!/bin/sh -eu

envUuid="$1"

# parallels qcow2 raw vdi vhdx vmdk vpc
targetFormat="${2:-qcow2}"

base="/eaas"

getMetadataPath() {
  echo "$base"/image-archive/meta-data/*/"$1".xml
}

showMetadata() {
  printf 'ID: %s\n' "$(basename -- "${1%.*}")"
  printf 'Name: '
  grep -Eo ':title>[^<]+' -- "$1" | cut -d'>' -f2
  printf 'Modification date: '
  grep -Eo ':timestamp>[^<]+' -- "$1" | cut -d'>' -f2
}

getImageUuids() {
  grep -Eo ':imageId>[^<]+' -- "$1" | cut -d'>' -f2
}

exitIfExists() {
  if test -e "$1"; then
    printf 'Error: %s already exists\n' "$1" >&2
    exit 1
  fi
}

if ! printf %s "$envUuid" | grep -Eq '^[0-9a-f-]+$'; then
  printf 'Error: invalid environment UUID: %s\n' "$envUuid" >&2
  exit 2
fi

metadata="$(getMetadataPath "$envUuid")"
showMetadata "$metadata" >&2

exportPath="/eaas/export/$envUuid.xml"
exitIfExists "$exportPath"
printf '%s\n' "$exportPath"
sudo cp -- "$metadata" "$exportPath"

i="0"
getImageUuids "$metadata" | while read -r imageUuid; do
  : "$((i += 1))"
  exportPath="$base/export/${envUuid}_disk${i}.${targetFormat}"
  exitIfExists "$exportPath"
  printf '%s\n' "$exportPath"
  sudo docker exec eaas qemu-img convert -O "$targetFormat" "http://nginx:81/$imageUuid" "$exportPath"
done
