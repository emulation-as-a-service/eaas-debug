#!/bin/sh -xeu

host="$1"
uuid="$2"

baseUrl="$host"
baseUrl="${baseUrl#https://}"
baseUrl="${baseUrl%%/*}"
baseUrl="https://$baseUrl"
baseUrl="$baseUrl/emil/components/resolve/unknown"

imageUrl="$baseUrl/$uuid"
curl -L "$imageUrl" -o "$uuid"

backingFile="$(qemu-img info --output json "$uuid" | jq -r '.["backing-filename"]')"
if test "$backingFile" && test "$backingFile" != "null"; then
  "$0" "$host" "$backingFile"
fi
