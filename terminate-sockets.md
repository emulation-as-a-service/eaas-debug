```shell
ss -nt dport = 443 and dst inet:127.0.0.1 -K
```

For reference, via strace, ss -K generates
`read(5, 0x558d3c8db860, 8192) = -1 ECONNABORTED (Software caused connection abort)`
on the targeted (client) side and
`read(6, 0x56406978e530, 8192) = -1 ECONNRESET (Connection reset by peer)` on
the other side.
