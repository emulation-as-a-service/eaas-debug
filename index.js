// Usage:
// import("//eaas.dev")

// Source: https://gitlab.com/emulation-as-a-service/eaas-debug/-/blob/main/index.js

// curl https://eaas.dev -vHsec-fetch-dest:script
// https://eaas.dev/index.js

export * from "./demo-ui-toolbox.js";
