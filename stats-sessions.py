#!/usr/bin/env python3

import sys
import csv
import argparse
from glob import glob

parser = argparse.ArgumentParser(
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument(
    "--filename", default=(glob("/eaas*/server-data/sessions.csv") or [None])[0], help=" ")
parser.add_argument("action", choices=["sessions", "max-sessions"])
args = parser.parse_args()


def fromtimestamp(timestamp):
    from datetime import datetime, timezone
    return datetime.fromtimestamp(int(timestamp)/1000, tz=timezone.utc)


out = csv.writer(sys.stdout, delimiter="\t")

id = 0
events = []
sessions = []

with open(args.filename, newline="") as file:
    for start, end, _, environment, *_ in csv.reader(file, delimiter=";"):
        if environment == "null":
            continue
        id += 1
        start = fromtimestamp(start)
        end = fromtimestamp(end)
        duration = end - start
        events += [(start, "start", id), (end, "end", id)]
        sessions += [(start, environment, duration.total_seconds())]

events.sort(key=lambda time, *_: time)

max = 0
maxx = []
for time, event, *_ in events:
    if event == "start":
        max += 1
    elif event == "end":
        max -= 1
    maxx += [(time, max)]

if args.action == "sessions":
    out.writerows(sessions)
elif args.action == "max-sessions":
    out.writerows(maxx)
