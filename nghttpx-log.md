Add some logging to nghttpx:

```shell
printf '\naccesslog-file=/nghttpx.log\naccesslog-write-early=yes\n' >> /etc/nghttpx/nghttpx.conf && sv restart nghttpx
```

or

```shell
sudo docker exec eaas sh -c "printf '\naccesslog-file=/home/bwfla/bw-fla-server/standalone/log/nghttpx.log\naccesslog-write-early=yes\n' >> /etc/nghttpx/nghttpx.conf && sv restart nghttpx"

sudo tail -f /eaas-home/log/server/nghttpx.log | grep ws+ethernet
```
