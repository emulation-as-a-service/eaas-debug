// Usage:
// import("//eaas.dev")

// Source: https://gitlab.com/emulation-as-a-service/eaas-debug/-/blob/main/demo-ui-toolbox.js

// curl https://eaas.dev -vHsec-fetch-dest:script
// https://eaas.dev/index.js

export function removeOverlays() {
  document.querySelectorAll("[_eaas_debug]").forEach((v) => v.remove());
}

export function once(fn) {
  let done = false,
    value;
  return new Proxy(fn, {
    apply(...a) {
      if (!done) {
        value = Reflect.apply(...a);
        done = true;
      }
      return value;
    },
  });
}

export const dateString = (timestamp) => {
  const date = new Date(timestamp);
  const hours = date.getUTCHours().toString().padStart(2, "0");
  const minutes = date.getUTCMinutes().toString().padStart(2, "0");
  const seconds = date.getUTCSeconds().toString().padStart(2, "0");
  const milliseconds = date.getUTCMilliseconds().toString().padStart(3, "0");

  return `${hours}:${minutes}:${seconds}.${milliseconds}`;
};

export const generateWebVTT = (events) => {
  let webVTTContent = "WEBVTT FILE\n\n";

  for (const [index, event] of events.entries()) {
    webVTTContent += `${index}
${dateString(event.time)} --> ${dateString(event.time + 1000)}
${JSON.stringify(event)}\n\n`;
  }

  return webVTTContent;
};

export class Recorder {
  parts = [];
  constructor(
    canvas = client()?.activeView?.container?.querySelector(
      "canvas",
    ),
    audio = client()?.activeView?._audioStream,
    guacamoleClient = client()?.activeView?.viewerInstance,
  ) {
    this.canvas = canvas;
    const stream = this.canvas.captureStream();
    if (globalThis.eaasVoiceStream) {
      stream.addTrack(globalThis.eaasVoiceStream);
    } else {
      audio?.getTracks()?.forEach((v) => stream.addTrack(v));
    }
    const recorder = (this.recorder = new MediaRecorder(stream));

    recorder.onstart = () => (this.startTime = performance.now());
    recorder.onstop = () => (this.stopTime = performance.now());
    recorder.onpause = () => (this.pauseTime = performance.now());
    recorder.onresume =
      () => (this.startTime -= performance.now() - this.pauseTime);
    recorder.ondataavailable = (ev) => this.parts.push(ev.data);

    this.events = [];
    if (globalThis.XpraClient) {
      before(XpraClient.prototype).send = ([type, ...args]) => {
        const time = this.time;
        switch (type) {
          case "key-action":
            {
              const [_, key, pressed] = args;
              this.events.push({
                time,
                event: pressed ? "keydown" : "keyup",
                key,
              });
            }
            break;
          case "button-action":
            {
              const [_, button, pressed, [x, y]] = args;
              this.events.push({
                time,
                event: pressed ? "mousedown" : "mouseup",
                x,
                y,
                // https://w3c.github.io/uievents/#dom-mouseevent-button
                button: button - 1,
              });
            }
            break;
          case "pointer-position":
            {
              const [_, [x, y]] = args;
              this.events.push({
                time,
                event: "mousemove",
                x,
                y,
              });
            }
            break;
        }
      };
    } else if (guacamoleClient) {
      let oldPressed = false;
      before(guacamoleClient).sendMouseState = ({
        x,
        y,
        left: pressed,
      }) => {
        const time = this.time;
        if (pressed != oldPressed) {
          this.events.push({
            time,
            event: pressed ? "mousedown" : "mouseup",
            x,
            y,
          });
        }
        oldPressed = pressed;
        this.events.push({ time, event: "mousemove", x, y });
      };
      before(guacamoleClient).sendKeyEvent = (pressed, charCode) => {
        const time = this.time;
        const key = String.fromCharCode(charCode);
        this.events.push({
          time,
          event: pressed ? "keydown" : "keyup",
          key,
        });
      };
    }

    recorder.start();
  }
  getEvents() {
    return generateWebVTT(this.events);
  }
  async stop() {
    this.recorder.stop();
    await new Promise((resolve) =>
      this.recorder.addEventListener("stop", resolve, { once: true })
    );
    this.fileName = `eaas-recording-${new Date().toISOString()}.mkv`;
    return new File(this.parts, this.fileName);
  }
  static download(blob, download = blob.name) {
    const href = URL.createObjectURL(blob);
    const el = document.head.appendChild(
      Object.assign(document.createElement("a"), { download, href }),
    );
    el.click();
    el.remove();
    URL.revokeObjectURL(href);
  }
  get time() {
    return performance.now() - this.startTime;
  }
}

export async function recordVoice() {
  const track = await navigator.mediaDevices.getUserMedia({ audio: true });
  globalThis.eaasVoiceStream = track.getAudioTracks()[0];
}
export async function stopVoice() {
  globalThis.eaasVoiceStream?.stop();
  globalThis.eaasVoiceStream = undefined;
}
export function record() {
  globalThis.eaasRecorder = new Recorder();
}
export async function recordSTT() {
  const speechRecognition = globalThis.eaasRecorder.speechRecognition =
    new (globalThis.SpeechRecognition ?? globalThis.webkitSpeechRecognition)();
  speechRecognition.continuous = true;
  speechRecognition.onresult = ({ results, resultIndex, timeStamp }) => {
    console.log(results[resultIndex][0].transcript);
    const rec = globalThis.eaasRecorder;
    rec.events.push({
      time: rec.time,
      event: "voice-annotation",
      annotation: results[resultIndex][0].transcript,
    });
  };
  speechRecognition.onerror = console.error;
  speechRecognition.start();
}
export async function recordStop() {
  globalThis.eaasRecorder.speechRecognition?.stop();
  const file = await globalThis.eaasRecorder.stop();
  Recorder.download(file);
}
export function recordAnnotation(
  recorder = globalThis.eaasRecorder,
  time = recorder.time,
) {
  const annotation = prompt(`Custom annotation at ${time / 1000} s:`);
  recorder.events.push({
    time,
    event: "annotation",
    annotation,
  });
}
export function downloadWebVTT() {
  Recorder.download(
    new File(
      [eaasRecorder.getEvents()],
      eaasRecorder.fileName.replace(/\.[^.]+$/, ".vtt"),
    ),
  );
}

const optionalCall = (maybeFn, thisArg) =>
  typeof maybeFn === "function" ? maybeFn.call(thisArg) : maybeFn;

export class DebugClient {
  get idToken() {
    return optionalCall(this.client.idToken, this.client);
  }

  constructor(
    _client = client(),
  ) {
    this.client = _client;
    this.baseUrl = String(_client.API_URL).replace(/\/*$/, "/");
  }

  async fetch(url, { method = "GET" } = {}) {
    return await (
      await fetch(
        new URL(String(url).replace(/^\/+/, ""), this.baseUrl),
        {
          method,
          headers: {
            authorization: `bearer ${await this.idToken}`,
          },
        },
      )
    ).json();
  }
}

const uuidRegExp =
  /[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/;
const pseudoUuidRegExp = /[0-9a-f-]{20,}/;

export const getFile = (id) =>
  new Request(
    `/emil/environment-repository/images/${encodeURIComponent(id)}/url`,
    {},
  );
export const getBackingFile = async (id) =>
  (
    await (
      await fetch(
        new Request(getFile(id), { headers: { range: "bytes=0-1023" } }),
      )
    ).text()
  ).match(pseudoUuidRegExp)?.[0];
export const getBackingChain = async (id) =>
  id ? [id, ...(await getBackingChain(await getBackingFile(id)))] : [];
export const formatBackingChain = async (id) => {
  if (!id) return document.createElement("div");
  const chain = await getBackingChain(id);
  const as = chain.flatMap((v) => {
    const a = document.createElement("a");
    a.href = getFile(v).url;
    a.textContent = v;
    //a.target = "_blank";
    a.download = "";
    const li = document.createElement("li");
    li.append(a);
    return [li, " "];
  });
  const display = document.createElement("ol");
  Object.assign(display.style, {
    //position: "absolute",
    //top: "20px",
    //left: 0,
    //width: "60ch",
    //opacity: 1,
    //zIndex: 999999,
  });
  const h1 = document.createElement("h4");
  h1.textContent = "Download Layers:";
  display.append(h1, ...as);
  return display;
};

export const getMediaId = (el) =>
  el.innerText.match(/Media ID: ([0-9a-f-]{20,})\s*(boot drive)?/)?.[1];
export const getOne = (el) => formatBackingChain(getMediaId(el));

export function* getDrives() {
  for (
    const drive of document.querySelectorAll(
      '[ng-repeat="drive in $ctrl.drives"]',
    )
  ) {
    let id = getMediaId(drive);
    if (!id) {
      const data = angular.element(drive).scope().drive?.data;
      console.log(data);
      id = data?.match(/^binding:\/\/(.+)/)?.[1];
    }
    yield { id, drive };
  }
}

export const showLayers = async () => {
  for (const { drive, id } of getDrives()) {
    drive.append(await formatBackingChain(id));
  }
};

const promptApiSecret = () =>
  prompt(
    "internalApiSecret\n\ngrep internalApiSecret /eaas*/config/eaas-config.d/01-main.yaml\n",
  );

export function convertToDiskImage() {
  const internalApiSecret = promptApiSecret();
  for (const { drive, id } of getDrives()) {
    if (!id) continue;
    const button = element(
      `<button type="button">Make available as disk image</button>`,
    );
    button.onclick = async () => {
      const label = prompt(`label for ${id}`);
      if (label) {
        await updateImage(id, label, internalApiSecret);
      }
    };
    drive.append(button);
  }
}

export async function deleteEmulator() {
  const internalApiSecret = promptApiSecret();
  const emulatorId = prompt(
    "⚠️ Dangerous!: ️️️Emulator metadata ID to delete (not exposed via UI, see /tmp/eaas-minio/image-archive/metadata/emulators/):",
  );
  await deleteEmulator2(emulatorId, internalApiSecret);
}

export const getJwt = async (
  key,
  body,
  header = { alg: "HS256", typ: "JWT" },
) => {
  const b64url = (string) =>
    btoa(string)
      .replaceAll("+", "-")
      .replaceAll("/", "_")
      .replaceAll("=", "");
  const hs256 = async (key, msg) =>
    String.fromCharCode(
      ...new Uint8Array(
        await crypto.subtle.sign(
          "hmac",
          await crypto.subtle.importKey(
            "raw",
            new TextEncoder().encode(key),
            { name: "hmac", hash: "sha-256" },
            false,
            ["sign"],
          ),
          new TextEncoder().encode(msg),
        ),
      ),
    );
  const signed = `${b64url(JSON.stringify(header))}.${
    b64url(
      JSON.stringify(body),
    )
  }`;
  return `${signed}.${b64url(await hs256(key, signed))}`;
};

export const getToken = (internalApiSecret) =>
  getJwt(internalApiSecret, {
    iss: "eaas",
    exp: Math.floor(Date.now() / 1000) + 60,
  });

export const updateImage = async (id, label, internalApiSecret) =>
  (
    await fetch(
      `/image-archive/v2/metadata/images/${encodeURIComponent(id)}`,
      {
        body: JSON.stringify({
          kind: "image/v1",
          id,
          category: "user",
          label,
        }),
        method: "PUT",
        headers: {
          "content-type": "application/json",
          authorization: `Bearer ${await getToken(
            internalApiSecret,
          )}`,
        },
      },
    )
  ).ok;

export const deleteEmulator2 = async (id, internalApiSecret) =>
  (
    await fetch(
      `/image-archive/v2/metadata/emulators/${encodeURIComponent(id)}`,
      {
        method: "DELETE",
        headers: {
          authorization: `Bearer ${await getToken(
            internalApiSecret,
          )}`,
        },
      },
    )
  ).ok;

export const getMetadata = async (type, id, internalApiSecret) =>
  (
    await fetch(
      `/image-archive/v2/metadata/${
        encodeURIComponent(
          type,
        )
      }/${encodeURIComponent(id)}`,
      {
        headers: {
          authorization: `Bearer ${await getToken(
            internalApiSecret,
          )}`,
        },
      },
    )
  ).json();

export const putMetadata = async (type, id, object, internalApiSecret) =>
  await fetch(
    `/image-archive/v2/metadata/${
      encodeURIComponent(
        type,
      )
    }/${encodeURIComponent(id)}`,
    {
      method: "PUT",
      body: JSON.stringify(object),
      headers: {
        "content-type": "application/json",
        authorization: `Bearer ${await getToken(internalApiSecret)}`,
      },
    },
  );

export const makeEnvironmentShared = async (id, internalApiSecret) => {
  const env = await getMetadata("environments", id, internalApiSecret);
  delete env.owner;
  (env.permissions ?? {}).user = "READ";
  await putMetadata("environments", id, env, internalApiSecret);
};

export const currentEnvId = () =>
  angular.element(document.querySelector("[ui-view=wizard]")).scope()
    .editEnvCtrl.env.envId;

export const makeEnvShared = async () => {
  const id = currentEnvId();
  if (
    !confirm(
      `⚠️ Make this environment (${id}) public by removing its owner/changing its owner to "shared"? 🔨`,
    )
  ) {
    return;
  }
  const internalApiSecret = promptApiSecret();
  await makeEnvironmentShared(id, internalApiSecret);
};

export const getImage = async (id, internalApiSecret) =>
  (
    await fetch(
      `/image-archive/v2/metadata/images/${encodeURIComponent(id)}`,
      {
        headers: {
          authorization: `Bearer ${await getToken(
            internalApiSecret,
          )}`,
        },
      },
    )
  ).json();

class Output {
  constructor({
    width = "60ch",
    height = undefined,
    opacity = 0.5,
    background = "gray",
  } = {}) {
    const out = (this.out = document.createElement("pre"));
    out.style.background = background;

    const iframe = (this.iframe = document.createElement("iframe"));
    Object.assign(iframe.style, { width, height, opacity });
    iframe.setAttribute("_eaas_debug", "");
    iframe.style.position = "fixed";
    iframe.style.top = 0;
    iframe.style.right = 0;
    iframe.style.zIndex = 999999;
    document.body.append(iframe);
  }

  set textContent(text) {
    this.out.textContent = text;
    this.iframe.srcdoc = this.out.outerHTML;
  }

  get textContent() {
    return this.out.textContent;
  }
}

export async function log() {
  const debugClient = new DebugClient();
  Object.assign(globalThis, { debugClient });
  const { stdout } = await debugClient.fetch(
    `components/${debugClient.client.activeView.componentId}/controlurls`,
  );
  const output = new Output({
    height: "100vh",
    background: "#80808040",
    opacity: 1,
  });
  (await fetch(stdout)).body.pipeThrough(new TextDecoderStream()).pipeTo(
    new WritableStream({
      write(v) {
        output.textContent += v;
      },
    }),
  );
}

export function overlay() {
  const out = new Output();

  const gcloudSsh = (hostname) =>
    `gcloud compute ssh "$(gcloud compute instances list --format "get(selfLink)" --filter="networkInterfaces.accessConfigs[].natIP=$(dig +short @1.0.0.1 ${hostname})" | tee /dev/stderr)"`;

  setInterval(() => {
    const eaasClient = client();
    const session = eaasClient?.sessions?.[0];
    const emucomp = new URL(
      session?.connectViewerUrl ??
        session?.eventSource?.url ??
        "https://invalid",
    ).hostname;
    const info = {
      sessionId: session?.componentId,
      emucomp,
      xpraUrl: eaasClient?.activeView?.xpraClient?.uri,
      sshGcloudGateway: gcloudSsh(location.hostname),
      sshGcloud: gcloudSsh(emucomp),
      sshEmucomp: `ssh -- ubuntu@${emucomp}`,
    };
    const text = Object.entries(info)
      .map(([k, v]) => `${k}= ${v}`)
      .join("\n");
    if (out.textContent !== text) {
      out.textContent = text;
    }

    globalThis.client ??= angular
      .element(document.querySelector("[ui-view=wizard]"))
      ?.scope()?.startEmuCtrl?.eaasClient?.activeView;
  }, 1000);

  globalThis.debug = {
    get topwindow() {
      return client.xpraClient.id_to_window[client.xpraClient.topwindow];
    },
  };
}

const html = (innerHTML) =>
  Object.assign(document.createElement("template"), { innerHTML }).content;
const element = (innerHTML) => html(innerHTML).firstChild;
const parts = (documentFragment) =>
  new Proxy(documentFragment, {
    get(target, propertyKey, receiver) {
      // TODO: properly escape propertyKey
      return target.querySelector(`[${propertyKey}]`);
    },
  });

export const client = () =>
  globalThis.angular?.element(document.querySelector("[ui-view=wizard]"))
    ?.scope()
    ?.startEmuCtrl?.eaasClient ??
    document.querySelector(".ai-emulator > div")?.__vue__?._data?.client;
export const xpraClient = (_client = client()) => _client.activeView.xpraClient;

export const reconnect = (_xpraClient = xpraClient()) => {
  _xpraClient.file_transfer = true;
  _xpraClient.do_reconnect();
};

export function xterm(_xpraClient = xpraClient()) {
  _xpraClient.start_command("", ["xterm", "bash"], []);
}

export function downloadFile(_xpraClient = xpraClient()) {
  const messages = ["Remote file path to download:"];
  if (!xpraClient().file_transfer) {
    messages.unshift(
      `⚠️ Warning: Might not work, then import(${
        JSON.stringify(
          import.meta.url,
        )
      }) before starting emulator or click reconnect.`,
    );
  }
  const path = prompt(messages.join("\n\n"));
  _xpraClient.start_command(
    "",
    [
      "xpra",
      "control",
      "/emucon/data/sockets/xpra-iosocket",
      "send-file",
      path,
    ],
    [],
  );
}

export const proxy = (object) =>
  new Proxy(object, {
    set: (target, property, apply, receiver) =>
      Reflect.set(
        target,
        property,
        new Proxy(Reflect.get(target, property, receiver), { apply }),
        receiver,
      ),
  });

export const before = (object) =>
  new Proxy(object, {
    set: (target, property, apply, receiver) =>
      Reflect.set(
        target,
        property,
        new Proxy(Reflect.get(target, property, receiver), {
          apply: (...a) => (apply(...a[2]), Reflect.apply(...a)),
        }),
        receiver,
      ),
  });

export const patchXpraFileTransfer = () => {
  if (!globalThis.XpraClient) return false;
  if (XpraClient.prototype.file_transfer) return true;
  return Object.defineProperty(XpraClient.prototype, "file_transfer", {
    get() {
      return true;
    },
    set() {},
  });
};

export const toggleRelativeMouse = (client = xpraClient()) =>
  client.forceRelativeMouse = !client.forceRelativeMouse;

if (!globalThis._loadXpraPatchingDone) {
  globalThis._loadXpraPatchingDone = true;
  globalThis._loadXpra = globalThis.loadXpra;
  Object.defineProperty(globalThis, "loadXpra", {
    set(v) {
      this._loadXpra = v;
    },
    get() {
      patchXpraFileTransfer();
      return this._loadXpra;
    },
  });
}

async function addRecordButton() {
  const option = document.querySelector(
    ".ah-options-right > span:nth-child(1)",
  );
  if (option) {
    document.querySelector("#eaas-debug-record-button")?.remove();
    const button = html(
      `<span class="ui-btn-container"><button id="eaas-debug-record-button" class="eaasi-button default sm" _button><span aria-hidden="true" class="fas fa-film eb-icon" _icon></span> Record Interactions</button></span>`,
    );
    const { _button, _icon } = parts(button);
    _button.onclick = async () => {
      if (!globalThis.eaasRecorder) {
        record();
        _button.style.color = "red";
        _icon.classList.remove("fa-film");
        _icon.classList.add("fa-stop");
      } else {
        await recordStop();
        await downloadWebVTT();
        globalThis.eaasRecorder = undefined;
        _button.style.color = "";
        _icon.classList.remove("fa-stop");
        _icon.classList.add("fa-film");
      }
    };
    option.after(button);
  }
}

const menu = {
  overlay,
  showLayers,
  log,
  recordVoice,
  stopVoice,
  record,
  recordSTT,
  recordAnnotation,
  recordStop,
  downloadWebVTT,
  addRecordButton,
  convertToDiskImage,
  makeEnvShared,
  deleteEmulator,
  removeOverlays,
  xterm,
  downloadFile,
  reconnect,
  toggleRelativeMouse,
};

import(import.meta.url).then((v) => (globalThis.eaasDebug = v));

if (globalThis.document) {
  const overlay = html(
    "<details id='eaas-debug-menu' _details style='position: fixed; top: 0; left 0; background: #fffff0f0; max-width: 250px; z-index: 1000;'><summary _summary>EaaS debug menu</summary></details>",
  );
  const { _details, _summary } = parts(overlay);
  for (const [name, fn] of Object.entries(menu)) {
    const el = html("<button type=button></button>").firstChild;
    el.textContent = name;
    el.onclick = () => fn();
    _details.append(el);
  }
  document.querySelector("#eaas-debug-menu")?.remove();
  document.body.append(overlay);
}
