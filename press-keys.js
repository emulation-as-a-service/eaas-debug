async function pressKeys(keys) {
    const pressKey = async (
        key,
        keyCode = key.toUpperCase().charCodeAt(0),
        { altKey, ctrlKey, metaKey, shiftKey, timeout } = { timeout: 100 },
        el = document.getElementById("emulator-container").firstElementChild
    ) => {
        if (el) {
            el.dispatchEvent(
                new KeyboardEvent("keydown", {
                    key,
                    keyCode,
                    ctrlKey,
                    altKey,
                    metaKey,
                    shiftKey,
                    bubbles: true,
                })
            );
            await new Promise((r) => setTimeout(r, 50));
            el.dispatchEvent(
                new KeyboardEvent("keypress", {
                    key,
                    keyCode,
                    ctrlKey,
                    altKey,
                    metaKey,
                    shiftKey,
                    bubbles: true,
                })
            );
            await new Promise((r) => setTimeout(r, 50));
            el.dispatchEvent(
                new KeyboardEvent("keyup", {
                    key,
                    keyCode,
                    ctrlKey,
                    altKey,
                    metaKey,
                    shiftKey,
                    bubbles: true,
                })
            );
        }
    };

    for (const key of await keys) pressKey(key);
}
