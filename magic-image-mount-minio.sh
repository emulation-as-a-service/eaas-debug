#!/bin/sh -eu
self="$(realpath -- "$0" || printf %s "%0")"

bucket="${1-image-archive}"

mkdir -p mnt
fusermount -u mnt || :

"${self%/*}"/minio-cred rclone mount --read-only ":s3:/$bucket/" mnt &
while ! mountpoint mnt; do sleep 1; done

mkdir -p images
ln -srf mnt/images/* mnt/public/images/* images/

wait
