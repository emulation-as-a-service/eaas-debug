#!/bin/sh -eu

bucket="$1"

mkdir -p mnt
fusermount -u mnt || :

rclone --gcs-service-account-file credentials.json mount --read-only ":gcs:/$bucket/" mnt &
while ! mountpoint mnt; do sleep 1; done

mkdir -p images
ln -srf mnt/images/* mnt/public/images/* images/

wait
