#!/usr/bin/env python3

import subprocess
import sys

import requests

INSTANCE_URL = "http://localhost:8080"
ORIGINAL_IMG_ID = ""


def get_url(image_id):
    return f"{INSTANCE_URL}/emil/environment-repository/images/{image_id}/url"


def determine_and_download_file(image_id):
    print("----- ----- -----")
    print("Starting new iteration for image id:", image_id)
    url = get_url(image_id)
    print("Using url:", url)

    ans = requests.get(url, allow_redirects=False)

    print("Got headers:", ans.headers)

    loc = ans.headers["Location"]
    print("Location:", loc)
    print(f"Downloading {image_id}...")
    subprocess.run(["curl", loc, "--output", image_id], check=True)
    print(f"Determining backing file for {image_id}...")
    output = subprocess.run(["qemu-img", "info", image_id], check=True,
                            stdout=subprocess.PIPE).stdout.decode().splitlines()

    backing_file_found = False
    for line in output:
        if line.startswith("backing file: "):
            new_id = line[14:].strip()
            print("Backing file id:", new_id)
            backing_file_found = True

    if backing_file_found:
        determine_and_download_file(new_id)
    else:
        print("No backing file specified, converting to standalone disk file...")
        subprocess.run(["qemu-img", "convert", "-O", "qcow2",
                       ORIGINAL_IMG_ID, "fullImage.img"], check=True)
        print("Finished, result: fullImage.img")


if __name__ == '__main__':

    print("This tool converts an image with all layers to a standalone qcow2 file.")
    print("Args: 1: Img Id, 2 (optional) backend URL (usually localhost works)")
    print("Run in eaas docker container!")

    ORIGINAL_IMG_ID = sys.argv[1]
    image_id = sys.argv[1]
    if len(sys.argv) > 2:
        INSTANCE_URL = sys.argv[2]
    determine_and_download_file(image_id)
