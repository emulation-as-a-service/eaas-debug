#!/usr/bin/env python3

import sys
import csv
from glob import glob


def strptime(date_string, format):
    import time
    import datetime
    return datetime.datetime(*(time.strptime(date_string, format)[0:6]), tzinfo=datetime.timezone.utc)

# with open("/eaas-home/server-data/sessions.csv")


out = csv.writer(sys.stdout, delimiter="\t")

with open(glob("/eaas*/log/server/nghttpx.log")[0], newline="") as file:
    for ip, _, _, dateString, _, request, _, _, referrer, *_ in csv.reader(file, delimiter=" "):
        if not request.startswith("POST /emil/components "):
            continue
        date = strptime(dateString, "[%d/%b/%Y:%H:%M:%S")
        out.writerow((date, ip, referrer))
