#!/usr/bin/env python

import sys, re
_, a, b, c = sys.argv

"""
sudo docker exec eaas ss -natupo > a.txt 
# start and end session
sudo docker exec eaas ss -natupo > b.txt 
# wait 2 min
sudo docker exec eaas ss -natupo > c.txt 
./filter.py a.txt b.txt c.txt
"""

class Item:
    def __init__(self, item, key):
        self.item, self.key = item, key
    def __eq__(self, other):
        return self.key == other.key
    def __hash__(self):
        return hash(self.key)

def get(file):
    with open(file) as f:
        return set(Item(line, re.sub(r"timer:\S*", "", re.sub(r"\s+", "\t", line.rstrip()))) for line in f)

print("".join(v.item for v in (get(b) & get(c)) - get(a)))
